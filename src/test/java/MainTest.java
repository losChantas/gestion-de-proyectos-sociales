import org.junit.Test;
import org.junit.Assert;

public class MainTest {

    @Test
    public void mainTest() {

        Main main = new Main();
        String[] args = new String[]{"Hello World"};
        String result = "Hello World";

        main.main(args);

        Assert.assertEquals("Hello World", result);
    }

}