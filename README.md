# README #

Este proyecto se basa en la gestion de proyectos sociales. 
Un trabajo practico contruido por la catedra de Diseño de Sistemas de la UTN.FRBA
## Instalación  ## 
* Instalar [JDK](https://docs.oracle.com/javase/8/docs/technotes/guides/install/install_overview.html) 1.8

* Instalar [Maven](https://www.educative.io/edpresso/how-to-install-maven-in-windows-and-linux-unix?aid=5082902844932096&utm_source=google&utm_medium=cpc&utm_campaign=edpresso-dynamic&gclid=EAIaIQobChMIwKrZi6rX6QIVVAmRCh0tRAkaEAAYASAAEgIKJPD_BwE) 3.6.X 

## Desarrollo 
* Build: Correr el comando ```mvn compile``` desde la command line
> Tambien se puede generar el build del proyecto desde IntelliJ 
>
```
Click derecho al Path Principal del proyecto -> Rebuild
```

* TEST: Correr el comando ```mvn test``` desde la command line

> Consideraciones
>
> Deberá agregar la librería de Junit a IntelliJ para poder correr los mismos.
> Se visualizará una notificación del IDE mencionado donde podrá agregar la libreria
* Dependencias: El manejo de una nueva dependencia se reliza agregando la misma al archivo ```pom.xml``` 
>
> Siempre que se agreguen nuevas dependencias correr el correr el comando ```mvn clean install``` 


### Creadores ###
* Grupo Los chantas
    - Martin Abelenda
    - Agustin Carabajal
    - Gonzalo Chimento
    - Delfina Bibe
    - Mariano Pololla
    